package ru.nsu.fit.endpoint.database;

import jersey.repackaged.com.google.common.collect.Lists;
import org.apache.commons.lang.Validate;
import org.slf4j.Logger;
import ru.nsu.fit.endpoint.database.data.CustomerPojo;
import ru.nsu.fit.endpoint.database.data.PlanPojo;
import ru.nsu.fit.endpoint.database.data.SubscriptionPojo;
import ru.nsu.fit.endpoint.shared.JsonMapper;

import java.sql.*;
import java.util.List;
import java.util.UUID;

public class DBService implements IDBService {
    // Constants
    private static final String INSERT_CUSTOMER = "INSERT INTO CUSTOMER(id, first_name, last_name, login, pass, balance) values ('%s', '%s', '%s', '%s', '%s', %s)";
    private static final String INSERT_SUBSCRIPTION = "INSERT INTO SUBSCRIPTION(id, customer_id, plan_id) values ('%s', '%s', '%s')";
    private static final String INSERT_PLAN = "INSERT INTO PLAN(id, name, details, fee) values ('%s', '%s', '%s', %s)";

    private static final String UPDATE_CUSTOMER = "UPDATE CUSTOMER SET first_name='%s', last_name='%s' WHERE id = '%s'";
    private static final String UPDATE_PLAN = "UPDATE PLAN SET name='%s', details='%s, fee='%s' WHERE id='%s'";

    private static final String DELETE_CUSTOMER = "DELETE CUSTOMER WHERE id = '%s'";
    private static final String DELETE_PLAN = "DELETE PLAN WHERE id = '%s'";
    private static final String DELETE_SUBSCRIPTION = "DELETE SUBSCRIPTION WHERE id = '%s'";

    private static final String SELECT_CUSTOMER_ID = "SELECT id FROM CUSTOMER WHERE id='%s'";
    private static final String SELECT_CUSTOMER = "SELECT id FROM CUSTOMER WHERE login='%s'";
    private static final String SELECT_CUSTOMERS = "SELECT * FROM CUSTOMER";
    private static final String SELECT_PLAN = "SELECT * FROM PLAN WHERE id='%s'";
    private static final String SELECT_PLANS = "SELECT * FROM PLAN";
    private static final String SELECT_SUBSCRIPTION_WITH = "SELECT * FROM SUBSCRIPTION WHERE customer_id = '%s' and plan_id = '%s'";
    private static final String SELECT_SUBSCRIPTIONS = "SELECT * FROM SUBSCRIPTION";
    private static final String TOP_UP_CUSTOMER_BALANCE = "UPDATE CUSTOMER SET balance=balance+'%s' WHERE id = '%s'";

    private Logger logger;
    private static final Object generalMutex = new Object();
    private Connection connection;

    public DBService(Logger logger) {
        this.logger = logger;
        init();
    }

    public CustomerPojo createCustomer(CustomerPojo customerData) {
        synchronized (generalMutex) {
            logger.info(String.format("Method 'createCustomer' was called with data: \n%s", JsonMapper.toJson(customerData, true)));

            customerData.id = UUID.randomUUID();
            try {
                Statement statement = connection.createStatement();
                statement.executeUpdate(
                        String.format(
                                INSERT_CUSTOMER,
                                customerData.id,
                                customerData.firstName,
                                customerData.lastName,
                                customerData.login,
                                customerData.pass,
                                customerData.balance));
                return customerData;
            } catch (SQLException ex) {
                logger.error(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public CustomerPojo updateCustomer(CustomerPojo customerData) {
        synchronized (generalMutex) {
            logger.info(String.format("Method 'updateCustomer' was called with data: \n%s", JsonMapper.toJson(customerData, true)));
            try {
                Statement statement = connection.createStatement();
                statement.executeUpdate(
                        String.format(
                                UPDATE_CUSTOMER,
                                customerData.firstName,
                                customerData.lastName,
                                customerData.id));
                return customerData;
            } catch (SQLException ex) {
                logger.error(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public void removeCustomer(UUID id) {
        synchronized (generalMutex) {
            logger.info(String.format("Method 'removeCustomer' was called with data: \n%s", id.toString()));
            try {
                Statement statement = connection.createStatement();
                statement.executeUpdate(String.format(DELETE_CUSTOMER, id));
            } catch (SQLException ex) {
                logger.error(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public void topUpBalance(UUID customerId, int amount) {
        synchronized (generalMutex) {
            logger.info(String.format("Method 'topUpBalance' was called with data: \n%s\n%s",
                    JsonMapper.toJson(customerId, true),
                    JsonMapper.toJson(amount, true)));
            try {
                Statement statement = connection.createStatement();
                statement.executeUpdate(
                        String.format(TOP_UP_CUSTOMER_BALANCE, amount, customerId));
            } catch (SQLException ex) {
                logger.error(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    @Override
    public void removeSubscription(UUID subscriptionId) {
        synchronized (generalMutex) {
            logger.info(String.format("Method 'removeSubscription' was called with data: \n%s", subscriptionId.toString()));
            try {
                Statement statement = connection.createStatement();
                statement.executeUpdate(String.format(DELETE_SUBSCRIPTION, subscriptionId));
            } catch (SQLException ex) {
                logger.error(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    @Override
    public List<SubscriptionPojo> getSubscription(UUID customerId, UUID planId) {
        synchronized (generalMutex) {
            logger.info("Method 'getSubscriptions' was called.");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(String.format(SELECT_SUBSCRIPTION_WITH, customerId, planId));
                List<SubscriptionPojo> result = Lists.newArrayList();
                while (rs.next()) {
                    SubscriptionPojo subscriptionData = new SubscriptionPojo();

                    subscriptionData.id = UUID.fromString(rs.getString(1));
                    subscriptionData.customerId = UUID.fromString(rs.getString(2));
                    subscriptionData.planId = UUID.fromString(rs.getString(3));

                    result.add(subscriptionData);
                }
                return result;
            } catch (SQLException ex) {
                logger.error(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    @Override
    public List<SubscriptionPojo> getSubscriptions(UUID customerId) {
        synchronized (generalMutex) {
            logger.info("Method 'getSubscriptions' was called.");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(SELECT_SUBSCRIPTIONS);
                List<SubscriptionPojo> result = Lists.newArrayList();
                while (rs.next()) {
                    SubscriptionPojo subscriptionData = new SubscriptionPojo();

                    subscriptionData.id = UUID.fromString(rs.getString(1));
                    subscriptionData.customerId = UUID.fromString(rs.getString(2));
                    subscriptionData.planId = UUID.fromString(rs.getString(3));

                    result.add(subscriptionData);
                }
                return result;
            } catch (SQLException ex) {
                logger.error(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public SubscriptionPojo createSubscription(SubscriptionPojo subscription) {
        synchronized (generalMutex) {
            logger.info(String.format("Method 'createSubscription' was called with data '%s'.", subscription));
            PlanPojo planPojo = getPlan(subscription.planId);
            Validate.isTrue(getSubscription(subscription.customerId, subscription.planId).isEmpty());
            int balance = getCustomer(subscription.customerId).balance;
            int fee =  - getPlan(subscription.planId).fee;
            Validate.isTrue(balance > fee);

            topUpBalance(subscription.customerId, -fee);
            subscription.id = UUID.randomUUID();
            try {
                Statement statement = connection.createStatement();
                statement.executeUpdate(
                        String.format(
                                INSERT_SUBSCRIPTION,
                                subscription.id,
                                subscription.customerId,
                                subscription.planId));
                return subscription;
            } catch (SQLException ex) {
                logger.error(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }


    public List<CustomerPojo> getCustomers() {
        synchronized (generalMutex) {
            logger.info("Method 'getCustomers' was called.");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(SELECT_CUSTOMERS);
                List<CustomerPojo> result = Lists.newArrayList();
                while (rs.next()) {
                    CustomerPojo customerData = new CustomerPojo();

                    customerData.id = UUID.fromString(rs.getString(1));
                    customerData.firstName = rs.getString(2);
                    customerData.lastName = rs.getString(3);
                    customerData.login = rs.getString(4);
                    customerData.pass = rs.getString(5);
                    customerData.balance = rs.getInt(6);

                    result.add(customerData);
                }
                return result;
            } catch (SQLException ex) {
                logger.error(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public CustomerPojo getCustomer(UUID customerId) {
        synchronized (generalMutex) {
            logger.info(String.format("Method 'getCustomerIdByLogin' was called with data '%s'.", customerId));

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        String.format(
                                SELECT_CUSTOMER_ID,
                                customerId));
                if (rs.next()) {
                    CustomerPojo customerData = new CustomerPojo();

                    customerData.id = UUID.fromString(rs.getString(1));
                    customerData.firstName = rs.getString(2);
                    customerData.lastName = rs.getString(3);
                    customerData.login = rs.getString(4);
                    customerData.pass = rs.getString(5);
                    customerData.balance = rs.getInt(6);

                    return customerData;
                } else {
                    throw new IllegalArgumentException("Customer with login '" + customerId + " was not found");
                }
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public UUID getCustomerIdByLogin(String customerLogin) {
        synchronized (generalMutex) {
            logger.info(String.format("Method 'getCustomerIdByLogin' was called with data '%s'.", customerLogin));

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        String.format(
                                SELECT_CUSTOMER,
                                customerLogin));
                if (rs.next()) {
                    return UUID.fromString(rs.getString(1));
                } else {
                    throw new IllegalArgumentException("Customer with login '" + customerLogin + " was not found");
                }
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public PlanPojo createPlan(PlanPojo plan) {
        synchronized (generalMutex) {
            logger.info(String.format("Method 'createPlan' was called with data '%s'.", plan));

            plan.id = UUID.randomUUID();
            try {
                Statement statement = connection.createStatement();
                statement.executeUpdate(
                        String.format(
                                INSERT_PLAN,
                                plan.id,
                                plan.name,
                                plan.details,
                                plan.fee));
                return plan;
            } catch (SQLException ex) {
                logger.error(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    @Override
    public PlanPojo updatePlan(PlanPojo plan) {
        synchronized (generalMutex) {
            logger.info(String.format("Method 'updatePlan' was called with data '%s'.", plan));
            try {
                Statement statement = connection.createStatement();
                statement.executeUpdate(
                        String.format(
                                UPDATE_PLAN,
                                plan.name,
                                plan.details,
                                plan.fee,
                                plan.id));
                return plan;
            } catch (SQLException ex) {
                logger.error(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    @Override
    public void removePlan(UUID id) {
        synchronized (generalMutex) {
            logger.info(String.format("Method 'removePlan' was called with data: \n%s", id.toString()));
            try {
                Statement statement = connection.createStatement();
                statement.executeUpdate(String.format(DELETE_PLAN, id));
            } catch (SQLException ex) {
                logger.error(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    @Override
    public List<PlanPojo> getPlans(UUID customerId) {
        synchronized (generalMutex) {
            logger.info("Method 'getPlans' was called.");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(SELECT_PLANS);
                List<PlanPojo> result = Lists.newArrayList();
                while (rs.next()) {
                    PlanPojo planPojo = new PlanPojo();

                    planPojo.id = UUID.fromString(rs.getString(2));
                    planPojo.name = rs.getString(2);
                    planPojo.details = rs.getString(3);
                    planPojo.fee = rs.getInt(4);

                    result.add(planPojo);
                }
                return result;
            } catch (SQLException ex) {
                logger.error(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    @Override
    public PlanPojo getPlan(UUID planId) {
        synchronized (generalMutex) {
            logger.info("Method 'getPlan' was called.");
            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(SELECT_PLAN);
                List<PlanPojo> result = Lists.newArrayList();
                while (rs.next()) {
                    PlanPojo planPojo = new PlanPojo();

                    planPojo.id = UUID.fromString(rs.getString(2));
                    planPojo.name = rs.getString(2);
                    planPojo.details = rs.getString(3);
                    planPojo.fee = rs.getInt(4);

                    result.add(planPojo);
                }
                return result.size() != 1 ? null : result.get(0);
            } catch (SQLException ex) {
                logger.error(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    private void init() {
        logger.debug("-------- MySQL JDBC Connection Testing ------------");
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            logger.debug("Where is your MySQL JDBC Driver?", ex);
            throw new RuntimeException(ex);
        }

        logger.debug("MySQL JDBC Driver Registered!");

        try {
            connection = DriverManager
                    .getConnection(
                            "jdbc:mysql://84.201.152.70:3306/testmethods?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&useSSL=false",
                            "user",
                            "user");
        } catch (SQLException ex) {
            logger.error("Connection Failed! Check output console", ex);
            throw new RuntimeException(ex);
        }

        if (connection != null) {
            logger.debug("You made it, take control your database now!");
        } else {
            logger.error("Failed to make connection!");
        }
    }
}
