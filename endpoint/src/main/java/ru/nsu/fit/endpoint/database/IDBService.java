package ru.nsu.fit.endpoint.database;

import ru.nsu.fit.endpoint.database.data.CustomerPojo;
import ru.nsu.fit.endpoint.database.data.PlanPojo;
import ru.nsu.fit.endpoint.database.data.SubscriptionPojo;

import java.util.List;
import java.util.UUID;

public interface IDBService {
    CustomerPojo createCustomer(CustomerPojo customerData);
    CustomerPojo updateCustomer(CustomerPojo customerData);


    CustomerPojo getCustomer(UUID customerId);
    List<CustomerPojo> getCustomers();
    PlanPojo getPlan(UUID planId);
    List<PlanPojo> getPlans(UUID customerId);
    List<SubscriptionPojo> getSubscription(UUID customerId, UUID planId);
    List<SubscriptionPojo> getSubscriptions(UUID customerId);

    UUID getCustomerIdByLogin(String customerLogin);

    PlanPojo createPlan(PlanPojo plan);
    PlanPojo updatePlan(PlanPojo plan);

    void removeCustomer(UUID id);
    void removePlan(UUID id);
    void removeSubscription(UUID subscriptionId);

    void topUpBalance(UUID customerId, int amount);

    SubscriptionPojo createSubscription(SubscriptionPojo subscription);
}
